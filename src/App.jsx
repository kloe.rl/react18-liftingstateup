import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      {/* Pass user value thru prop drilling */}
      <Navigation user={user}/>
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
