import "./Navigation.css";

function Navigation(props) {
  // Get user from props drilling
  const { user } = props;

  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* Check if user is connected and change text*/}
      <li>{user ? user?.name : 'Please log in'}</li>
    </ul>
  );
}

export default Navigation;
